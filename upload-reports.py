import requests
import sys
import os

def main():
    if len(sys.argv) < 2:
        print("Usage: upload-reports.py <file_name>")
        sys.exit(1)

    file_name = sys.argv[1]
    scan_type = ''

    # Determine the scan type based on the file name
    if file_name == 'gitleaks.json':
        scan_type = 'Gitleaks Scan'
    elif file_name == 'njsscan.sarif':
        scan_type = 'SARIF'
    elif file_name == 'semgrep.json':
        scan_type = 'Semgrep JSON Report'
    elif file_name == 'retire.json':
        scan_type = 'Retire.js Scan'
    elif file_name == 'trivy.json':
        scan_type = 'Trivy Scan'
    else:
        print(f"Unsupported file type: {file_name}")
        sys.exit(1)

    # Verify the file exists
    if not os.path.isfile(file_name):
        print(f"File not found: {file_name}")
        sys.exit(1)

    # Define headers and data for the request
    headers = {
        'Authorization': 'Token 462e7e96010de951ed3b968e8a69d3fc19ac3e27'
    }

    url = 'https://demo.defectdojo.org/api/v2/import-scan/'

    data = {
        'active': True,
        'verified': True,
        'scan_type': scan_type,
        'minimun_severity': 'low',
        'engagement': 21
    }

    try:
        # Open the file and make the POST request
        with open(file_name, 'rb') as f:
            files = {'file': f}
            response = requests.post(url, headers=headers, data=data, files=files)

        # Check the response status code
        if response.status_code == 201:
            print('Scan results imported successfully')
        else:
            print(f'Failed to import scan results: {response.status_code} - {response.content.decode()}')
    except Exception as e:
        print(f"An error occurred: {e}")
        sys.exit(1)

if __name__ == "__main__":
    main()
